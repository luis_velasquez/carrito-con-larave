<?php
/**
 * Created by PhpStorm.
 * User: lvelasquez
 * Date: 12/21/14
 * Time: 11:31 PM
 */
class Product extends Eloquent {

    protected $fillable = array(
        'category_id',
        'title',
        'description',
        'price',
        'availability',
        'image'
    );

    public static $rules = array(
        'category_id' => 'required | integer',
        'title' => 'required|min:2',
        'description' => 'required|min:20',
        'price' => 'required|numeric',
        'availability' => 'integer',
        'image' => 'required|image|mimes:jpeg,jpg,gif,png,bmp'
    );

    public function category() {
        return $this->belongsTo('Category');
    }
}