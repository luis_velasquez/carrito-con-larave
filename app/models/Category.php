<?php
/**
 * Created by PhpStorm.
 * User: lvelasquez
 * Date: 12/20/14
 * Time: 8:17 PM
 */
class Category extends  Eloquent {

    protected $fillable = array('name');

    public  static $rules = array('name'=>'required|min:3');

    public function products() {
        return $this->hasMany('Product');
    }
}