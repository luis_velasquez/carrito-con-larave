<?php
/**
 * Created by PhpStorm.
 * User: lvelasquez
 * Date: 12/22/14
 * Time: 11:13 PM
 */
class Availability {

    public static function display($availability) {
        if ($availability == 0) {
            echo "Out of stock";
        } else if ($availability == 1) {
            echo "In stock";
        }
    }

    public static function displayClass($availability) {
        if ($availability == 0) {
            echo "outofstock";
        } else if ($availability == 1) {
            echo "instock";
        }
    }
}