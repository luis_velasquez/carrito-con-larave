@extends('layouts.main')

@section('content')
    <section id="signin-form">
        <h1>I have an account</h1>
        {{ Form::open(array('url'=>'users/signin')) }}
            <p>
                {{ HTML::image('img/email.gif', 'Email Address') }}
                {{ Form::text('email') }}
            </p>
        <p>
            {{ HTML::image('img/password.gif', 'Password') }}
            {{ Form::password('password') }}
        </p>
        {{ Form::button('Sign in', array('type'=>'submit', 'class'=>'secondary-cart-btn')) }}
        {{ Form::close() }}
        </form>
    </section><!-- end signin-form -->
@stop