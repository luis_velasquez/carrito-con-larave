<?php
/**
 * Created by PhpStorm.
 * User: lvelasquez
 * Date: 12/28/14
 * Time: 11:35 AM
 */

class UsersTableSeeder extends Seeder {

    public function run() {
        $user = new User;
        $user->firstname = 'John';
        $user->lastname = 'DOe';
        $user->email = 'John@doe.com';
        $user->password = Hash::make('mypassword');
        $user->telephone = '777555123';
        $user->admin = 1;
        $user->save();
    }
}